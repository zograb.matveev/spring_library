package am.matveev.library.dao;

import am.matveev.library.config.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class PersonDAO{
    private  final JdbcTemplate jdbcTemplate;

    @Autowired
    public PersonDAO(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Person> allPeople(){
       return jdbcTemplate.query("SELECT * FROM Person",new BeanPropertyRowMapper<>(Person.class));
    }
    public Person showPerson(int id){
      return jdbcTemplate.query("SELECT * FROM Person WHERE id=?",new Object[] {id},
             new BeanPropertyRowMapper<>(Person.class)).stream().findAny().orElse(null);
    }
    public void save(Person person){
          jdbcTemplate.update("INSERT INTO Person(surname,name,patronymic,birthday) VALUES (?,?,?,?)",
                 person.getSurname(),person.getName(),person.getPatronymic(), person.getBirthday());
    }

    public void update(int id, Person updatedPerson){
      jdbcTemplate.update("UPDATE Person SET surname=?,name=?,patronymic=?,birthday=? WHERE id=?",
              updatedPerson.getSurname(),updatedPerson.getName(),updatedPerson.getPatronymic(),updatedPerson.getBirthday(),id);
    }

    public void delete(int id){
       jdbcTemplate.update("DELETE FROM Person WHERE id=?",id);
    }
}

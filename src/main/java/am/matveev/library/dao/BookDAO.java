package am.matveev.library.dao;

import am.matveev.library.config.models.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookDAO{
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public BookDAO(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Book> allBooks(){
        return jdbcTemplate.query("SELECT * FROM Book",new BeanPropertyRowMapper<>(Book.class));
    }
    public Book showBook(int id){
        return jdbcTemplate.query("SELECT * FROM Book WHERE id=?",new Object[] {id},
                new BeanPropertyRowMapper<>(Book.class)).stream().findAny().orElse(null);
    }
    public void save(Book book){
        jdbcTemplate.update("INSERT INTO Book(bookName,authorName,dayOfWrite) VALUES (?, ?, ?)",
                book.getBookName(),book.getAuthorName(),book.getDayOfWrite());
    }

    public void update(int id,Book updatedBook){
        jdbcTemplate.update("UPDATE Book SET bookName=?,authorName=?,dayOfWrite=? WHERE id=?"
        ,updatedBook.getBookName(),updatedBook.getAuthorName(),updatedBook.getDayOfWrite(),id);
    }

    public void delete(int id){
        jdbcTemplate.update("DELETE FROM Book WHERE id=?",id);
    }
}

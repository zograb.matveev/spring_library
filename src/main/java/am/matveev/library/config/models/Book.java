package am.matveev.library.config.models;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class Book{
    private int id;

    @NotEmpty(message = "BookName should not be empty")
    @Size(min=3,max=50 , message = "BookName should be between 3 and 50 characters")
    private String bookName;

    @NotEmpty(message = "AuthorName should not be empty")
    @Size(min=3,max=50 , message = "AuthorName should be between 3 and 50 characters")
    private String authorName;

    @Min(value = 1000,message = "DayOfWrite should be greater then 1000")
    private int dayOfWrite;

    public Book(int id, String bookName, String authorName, int dayOfWrite){
        this.id = id;
        this.bookName = bookName;
        this.authorName = authorName;
        this.dayOfWrite = dayOfWrite;
    }

    public Book(){

    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getBookName(){
        return bookName;
    }

    public void setBookName(String bookName){
        this.bookName = bookName;
    }

    public String getAuthorName(){
        return authorName;
    }

    public void setAuthorName(String authorName){
        this.authorName = authorName;
    }

    public int getDayOfWrite(){
        return dayOfWrite;
    }

    public void setDayOfWrite(int dayOfWrite){
        this.dayOfWrite = dayOfWrite;
    }
}

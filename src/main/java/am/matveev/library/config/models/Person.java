package am.matveev.library.config.models;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class Person{
    private int id;

    @NotEmpty(message = "Surname should not be empty ")
    @Size(min=1,max=30,message = "Surname should be between 1 and 30 characters")
    private String surname;

    @NotEmpty(message = "Surname should not be empty ")
    @Size(min=1,max=30, message = "Name should be between 1 and 30 characters")
    private String name;

    @NotEmpty(message = "Surname should not be empty ")
    @Size(min=1,max=30, message = "Patronymic should be between 1 and 30 characters")
    private String patronymic;

    @Min(value = 1940,message = "Birthday should be greater then 1940")
    private int birthday;


    public Person(int id, String surname, String name, String patronymic, int birthday){
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthday = birthday;
    }

    public Person(){

    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getSurname(){
        return surname;
    }

    public void setSurname(String surname){
        this.surname = surname;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getPatronymic(){
        return patronymic;
    }

    public void setPatronymic(String patronymic){
        this.patronymic = patronymic;
    }

    public int getBirthday(){
        return birthday;
    }

    public void setBirthday(int birthday){
        this.birthday = birthday;
    }
}
